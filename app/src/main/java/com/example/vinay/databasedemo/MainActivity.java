package com.example.vinay.databasedemo;

import android.app.AlertDialog;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by vinay on 4/09/17.
 */

public class MainActivity extends FragmentActivity {

    private EditText mEtFirstName;
    private EditText mEtLastName;
    private EditText mEtMarks;
    private EditText mETUpdateStudent;
    private EditText mETDeleteStudent;
    private Button mBtnInsert;
    private Button mBtnSHowAll;
    private Button mBtnUpdate;
    private Button mBtnDelete;
    private DatabaseHelper mDatabaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mDatabaseHelper = new DatabaseHelper(getApplicationContext());
        mEtFirstName = (EditText) findViewById(R.id.first_name);
        mEtLastName = (EditText) findViewById(R.id.last_name);
        mEtMarks = (EditText) findViewById(R.id.marks);
        mETUpdateStudent = (EditText) findViewById(R.id.update_id);
        mETDeleteStudent = (EditText) findViewById(R.id.delete_id);

        mBtnInsert = (Button) findViewById(R.id.insert);
        mBtnSHowAll = (Button) findViewById(R.id.show_all);
        mBtnUpdate = (Button) findViewById(R.id.update);
        mBtnDelete = (Button) findViewById(R.id.delete);
        mBtnInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                insertData();
            }
        });

        mBtnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                delete();
            }
        });
        mBtnSHowAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAll();
            }
        });
        mBtnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                update();
            }
        });
    }

    /**
     * Delete the student from database based on the ID specified.
     */
    private void delete() {
        if (!TextUtils.isEmpty(mETDeleteStudent.getText().toString())) {
            boolean res = mDatabaseHelper.delete(Integer.parseInt(mETDeleteStudent.getText().toString()));
            if (res == true) {
                Toast.makeText(getApplicationContext(), R.string.Success, Toast.LENGTH_SHORT).show();
                clearEditTexts();
            } else {
                Toast.makeText(getApplicationContext(), R.string.failure, Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getApplicationContext(), R.string.invalid_input, Toast.LENGTH_SHORT).show();
        }
        clearEditTexts();
    }

    /**
     * Update the student in the database based on the ID specified.
     */
    private void update() {
        if (!TextUtils.isEmpty(mETUpdateStudent.getText().toString()) && !TextUtils.isEmpty(mEtFirstName.getText().toString())
                && !TextUtils.isEmpty(mEtLastName.getText().toString()) && !TextUtils.isEmpty(mEtMarks.getText().toString())) {
            boolean res = mDatabaseHelper.update(Integer.parseInt(mETUpdateStudent.getText().toString()),
                    mEtFirstName.getText().toString(), mEtLastName.getText().toString(),
                    Integer.parseInt(mEtMarks.getText().toString()));
            if (res == true) {
                Toast.makeText(getApplicationContext(), R.string.Success, Toast.LENGTH_SHORT).show();
                clearEditTexts();
            } else {
                Toast.makeText(getApplicationContext(), R.string.failure, Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getApplicationContext(), R.string.invalid_input, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Fetch and Display all the students info in the database.
     */
    private void showAll() {
        Cursor cursor = mDatabaseHelper.getALLData();
        if (cursor.getCount() == 0) {
            showAlert(getString(R.string.error), getString(R.string.no_data));
        } else {
            StringBuffer stringBuffer = new StringBuffer();
            while (cursor.moveToNext()) {
                stringBuffer.append("\n ID = " + cursor.getString(0) + "\n First name = " + cursor.getString(1) + "\n Last name = "
                        + cursor.getString(2) + "\n Marks = " + cursor.getString(3) + "\n\n");
            }
            showAlert(getString(R.string.students_data), stringBuffer.toString());
        }

    }

    /**
     * Create a AlertDialog which displays the students info.
     *
     * @param error
     * @param s
     */
    private void showAlert(String error, String s) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(error);
        builder.setMessage(s);
        builder.show();
    }

    /**
     * Insert the new student info into the database.
     */
    private void insertData() {
        if (!TextUtils.isEmpty(mEtFirstName.getText().toString()) && !TextUtils.isEmpty(mEtLastName.getText().toString())
                && !TextUtils.isEmpty(mEtMarks.getText().toString())) {
            boolean res = mDatabaseHelper.insertDAta(mEtFirstName.getText().toString(), mEtLastName.getText().toString(),
                    Integer.parseInt(mEtMarks.getText().toString()));
            if (res == true) {
                Toast.makeText(getApplicationContext(), R.string.Success, Toast.LENGTH_SHORT).show();
                clearEditTexts();
            } else {
                Toast.makeText(getApplicationContext(), R.string.failure, Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getApplicationContext(), R.string.invalid_input, Toast.LENGTH_SHORT).show();
        }
    }

    private void clearEditTexts() {
        mEtFirstName.setText("");
        mEtLastName.setText("");
        mEtMarks.setText("");
        mETUpdateStudent.setText("");
        mETDeleteStudent.setText("");
    }

}

package com.example.vinay.databasedemo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
/**
 * Created by vinay on 4/09/17.
 */

/**
 * A helper class to manage database creation and version management.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "student.db";
    private static final String DATABASE_TABLE = "student_table";
    private static final String COL_ZERO_ID = "ID";
    private static final String COL_ONE_FIRST_NAME = "FIRST_NAME";
    private static final String COL_TWO_LAST_NAME = "LAST_NAME";
    private static final String COL_THREE_MARKS = "MARKS";
    private static final java.lang.String CREATE_TABLE = "create table " + DATABASE_TABLE + "(" + COL_ZERO_ID + " INTEGER primary key AUTOINCREMENT ," +
            COL_ONE_FIRST_NAME + " TEXT," + COL_TWO_LAST_NAME + " TEXT, " + COL_THREE_MARKS + " INTEGER);";

    private static final java.lang.String DROP_TABLE = "DROP Table " + DATABASE_TABLE;

    private static final java.lang.String SELECT_ALL = "select * from " + DATABASE_TABLE;


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(DROP_TABLE);
    }

    /**
     * Insert students data into the database.
     *
     * @param fn
     * @param ln
     * @param marks
     * @return
     */
    public boolean insertDAta(String fn, String ln, int marks) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_ONE_FIRST_NAME, fn);
        contentValues.put(COL_TWO_LAST_NAME, ln);
        contentValues.put(COL_THREE_MARKS, marks);
        long res = sqLiteDatabase.insert(DATABASE_TABLE, null, contentValues);
        if (res != 0)
            return true;
        else
            return false;
    }

    /**
     * Fetch all the students data in the database.
     *
     * @return Cursor
     */
    public Cursor getALLData() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        return sqLiteDatabase.rawQuery(SELECT_ALL, null);
    }

    /**
     * Update the student in the database based on the ID specified.
     *
     * @param id
     * @param fn
     * @param ln
     * @param marks
     * @return
     */
    public boolean update(int id, String fn, String ln, int marks) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_ONE_FIRST_NAME, fn);
        contentValues.put(COL_TWO_LAST_NAME, ln);
        contentValues.put(COL_THREE_MARKS, marks);
        long res = sqLiteDatabase.update(DATABASE_TABLE, contentValues, "ID = " + id, null);
        if (res == 1)
            return true;
        else
            return false;
    }

    /**
     * Delete the student from database based on the ID specified.
     *
     * @param id
     * @return
     */
    public boolean delete(int id) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        int re = sqLiteDatabase.delete(DATABASE_TABLE, "ID =" + id, null);
        if (re == 1) {
            return true;
        } else {
            return false;
        }
    }
}
